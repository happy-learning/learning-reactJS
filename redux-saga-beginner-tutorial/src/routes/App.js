import React from 'react'
import { connect } from 'react-redux'
import Counter from '../components/Counter'

const mapStateToProps = (state, ownProps) => {
  return {
    value: state.counter
  }
}

const action = (actionName) => {
  return {type: actionName}
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    onIncrement: () => dispatch(action('INCREMENT')),
    onDecrement: () => dispatch(action('DECREMENT')),
    onIncrementAsync: () => dispatch(action('INCREMENT_ASYNC')),
    onLogin: () => dispatch(
      {
        type: 'LOGIN_REQUEST', 
        user:'gulinrang', 
        password: '123456'
      }
    ),
    onLogout: () => dispatch(action('LOGOUT'))
  }
}

const App = connect(
  mapStateToProps,
  mapDispatchToProps
)(Counter)

export default App

