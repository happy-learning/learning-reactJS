import React from 'react'
import { connect } from 'react-redux'

class Aa extends React.Component {

  render() {
    return (
      <div>{this.props.value}</div>  
    )
  }
}

Aa = connect(
  state => ({
    value: state.counter
  })
)(Aa)

export default Aa