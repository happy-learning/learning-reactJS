export const counter = (state = 0, action) => {
  switch (action.type) {
    case 'INCREMENT':
      return state + 1
    case 'INCREMENT_IF_ODD':
      return (state % 2 !== 0) ? state + 1 : state
    case 'DECREMENT':
      return state - 1
    default:
      // console.log(action)
      return state
  }
}

export function loginOrLogout(state = {
      loginStatus: false,
      message: 'no message',
      token: ''
    }, action) {
  switch (action.type) {
    case 'LOGIN_SUCCESS':
      console.log('reducers >> action.type', action.type)
      console.log('reducers >> action.token', action.token)
      // ('action.token:', action.token)
      return {loginStatus: true, message: 'login is successful', token: action.token}
    case 'LOGIN_ERROR':
      console.log('reducers >> action.error',action.error)
      return {loginStatus: false, message: action.error}
    case 'LOGOUT':
      console.log('reducers >> action.type:', action.type)
      return {loginStatus: false, message: 'logout'}
    default:
      // console.log('action', action)
      return state
  }
}