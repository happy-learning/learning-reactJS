export default {
  fetch: (path) => {
    const data = {
      path: path
    }
    return new Promise((resolve, reject) => {
      resolve(data)
    })
  },
  authorize: (user, password) => {
  //   const promise = undefined
  //   const timer = setTimeout((promise) => {
  //     console.log('loading...')
  //     promise = new Promise((resolve, reject) => {
  //       console.log('this is authorize api')
  //       resolve('this is token')
  //     })
  //   }, 2000, promise)
  //   // clearTimeout(timer)
  //   // return promise
  // },
    // var startTime = new Date().getTime()
    // console.log('loading...')
    // while (new Date().getTime() < startTime + 20000);
    
    return new Promise((resolve, reject) => {
      console.log('Api.js >> this is authorize api')
      resolve('this is token')
    })
  },
  storeItem: ({token}) => {
    return new Promise((resolve, reject) => {
      console.log('Api.js >> token stored')
      resolve('token stored')
    })
  },
  clearItem: (token) => {
    return () => new Promise((resolve, reject) => {
      console.log('Api.js >> token cleared')
      resolve('token cleared')
    })
  }
}