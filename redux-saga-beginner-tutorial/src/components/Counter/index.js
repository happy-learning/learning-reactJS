/*eslint-disable no-unused-vars */
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Aa from '../../routes/Aa'

export default class Counter extends Component {
  render() {
    return  (
      <div>
        <button onClick={this.props.onIncrement}>
          Increment
        </button>
        {' '}
        <button onClick={this.props.onDecrement}>
          Decrement
        </button>
        {' '}
        <button onClick={this.props.onIncrementAsync}>
          Increment after 1 second
        </button>
        <hr />
        <div>
          Clicked: {this.props.value} times
        </div>
        {' '}
        <button onClick={this.props.onLogin}>
          Login
        </button>
        {' '}
        <button onClick={this.props.onLogout}>
          Logout
        </button>
        <Aa/>
      </div>
    )
  }
}

Counter.propTypes = {
  value: PropTypes.number.isRequired,
  onIncrement: PropTypes.func.isRequired,
  onDecrement: PropTypes.func.isRequired,
  onIncrementAsync: PropTypes.func.isRequired,
  onLogin: PropTypes.func.isRequired,
  onLogout: PropTypes.func.isRequired,
}