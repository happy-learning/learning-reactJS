import { delay } from 'redux-saga'
import {
  put,
  take,
  // takeEvery,
  all,
  call,
  fork,
  cancel,
  race,
  cancelled,
  select
} from "redux-saga/effects";
import Api from '../services/Api'

export const START_BACKGROUND_SYNC = 'START_BACKGROUND_SYNC'
export const STOP_BACKGROUND_SYNC = 'STOP_BACKGROUND_SYNC'

/***************************** Subroutines ************************************/

// Our worker Saga: will perform the async increment task
export function* incrementAsync() {
  // yield delay(1000)
  yield call(delay, 1000)
  yield put({type: 'INCREMENT'})
}

export function* fetchProducts() {
  try {
    const products = yield call(Api.fetch, './products')
    yield put({ type: 'PRODUCTS_RECEIVED', products })
  }
  catch(error) {
    yield put({ type: 'PRODUCTS_REQUEST_FAILED', error })
  }
}

export function* authorize(user, password) {
  try {
    // yield console.log('this is authorize function');
    yield call(delay, 1000)
    // const [token] = yield [call(Api.authorize, user, password),]
    const token = yield call(Api.authorize, user, password)
    yield put({type: 'LOGIN_SUCCESS', token})
    yield call(Api.storeItem, {token})
    return token
  } catch(error) {
    yield put({type: 'LOGIN_ERROR', error})
  } finally {
    if(yield cancelled()) {
      console.log('sagas >> authorize:  canceled')
    }
  }
}

export function* bgSync(a, b) {
  try {
    while (true) {
      // const result = yield call(someApi)
      // yield put(actions.requestSuccess(result))
      yield call(delay, 5000)
    }
  } finally {
    if (yield cancelled()) {
      console.log('Sync cancelled!')
      // yield put(actions.requestFailure('Sync cancelled!'))
    }
  }
}

export function* main() {
  while (true) {    
    const {user, password} = yield take(START_BACKGROUND_SYNC)
    // starts the task in the background
    const bgSyncTask = yield fork(bgSync, user, password)
    // wait for the user stop action
    yield take(STOP_BACKGROUND_SYNC)
    // user clicked stop. cancel the background task
    // this will cause the forked bgSync task to jump into its finally block
    yield cancel(bgSyncTask)
  }
}

/******************************************************************************/
/******************************* WATCHERS *************************************/
/******************************************************************************/
// Our watcher Saga: spawn a new incrementAsync task on each INCREMENT_ASYNC
export function* watchIncrementAsync() {
  while (true) {
    yield take('INCREMENT_ASYNC')
    yield call(delay, 1000)
    yield put({type: 'INCREMENT'})
    const counter = yield select(state => {
      // console.log(state)
      return state.counter
    })
    console.log(counter)
  }
  // yield takeEvery('INCREMENT_ASYNC', incrementAsync)
}

export function* watchFirstThreeTodosCreation() {
  for(let i = 0; i < 3; i++) {
    yield take('INCREMENT')
  }
  yield put({type: 'SHOW_CONGRATULATION'})
}

export function* watchShowCongratulation() {
  yield take('SHOW_CONGRATULATION')
  console.log('congatulations !!!')
}


export function* loginFlow() {
  while(true) {
    const {user, password} = yield take('LOGIN_REQUEST')
    const {token, timeout} = yield race({
      token: call(authorize, user, password),
      timeout: call(delay, 2000)
    })
    if (timeout) {
      console.log('loginFlow >> authorize api timeout')
    } else {
      console.log('loginFlow >> authorize ok', `\ntoken is : ${token}`)
    }
    yield take(['LOGOUT', 'LOGIN_ERROR'])
    yield call(Api.clearItem('token'))
  }
}

// export function* loginFlow() {
//   while(true) {
//     const {user, password} = yield take('LOGIN_REQUEST')
//     const task = yield fork(authorize, user, password)
//     const action = yield take(['LOGOUT', 'LOGIN_ERROR'])
//     if(action.type === 'LOGOUT') {
//       yield cancel(task)
//     }
//     yield call(Api.clearItem('token'))
//   }
// }


// single entry point to start all Sagas at once
export default function* rootSaga() {
  yield all([
    fork(watchIncrementAsync),
    fork(watchFirstThreeTodosCreation),
    fork(loginFlow),
    fork(watchShowCongratulation)
  ])
}

