import React from 'react'
import ReactDOM from 'react-dom'
import {
  // Router,
  Route,
  Switch
} from 'react-router-dom'
import { createStore, applyMiddleware, combineReducers, compose } from 'redux';
import createSagaMiddleware from 'redux-saga'
import { Provider } from 'react-redux'
import createHistory from 'history/createBrowserHistory'
import { 
  ConnectedRouter, 
  routerReducer as routing, 
  routerMiddleware 
} from 'react-router-redux'

import * as reducers from './reducers'
import rootSaga from './sagas'
import App from './routes/App'

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const sagaMiddleware = createSagaMiddleware()
const history = createHistory()
const store = createStore(
  combineReducers({
    ...reducers,
    routing
  }),
  composeEnhancers(
    applyMiddleware(sagaMiddleware, routerMiddleware(history))
  )
)
sagaMiddleware.run(rootSaga)


// ========使用Provider之后，子组件就可以通过connect去绑定store的内容=====================================
// ========并且store内state发生变化之后，自动渲染，而不需要手动通过store.subscribe去监听render函数=====================================
ReactDOM.render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <Switch>
        <Route path="/" component={App}/>
      </Switch>
    </ConnectedRouter>
  </Provider>,
  document.getElementById('root')
)