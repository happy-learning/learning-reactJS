import * as counter from './counter'
import * as todos from './todos'
import * as auth from './auth'

export default {
  ...counter,
  ...todos,
  ...auth
}