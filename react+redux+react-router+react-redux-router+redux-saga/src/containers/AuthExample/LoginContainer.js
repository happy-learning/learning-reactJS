import { connect } from 'react-redux'
import { push } from 'react-router-redux'
import LoginButton from '../../components/AuthExample/LoginButton'
import actions from '../../actions'

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    login: () => {
      dispatch(actions.authSuccess())
      dispatch(push('/'))
    }
  }
}

const LoginContainer = connect(
  null,
  mapDispatchToProps
)(LoginButton)

export default LoginContainer