import { connect } from 'react-redux'
import { push } from 'react-router-redux'
import HomeButton from '../../components/AuthExample/HomeButton'
import actions from '../../actions'

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    logout: () => {
      dispatch(actions.authFail())
      console.log(push('/login'))
      dispatch(push('/login'))
    }
  }
}

const HomeContainer = connect(
  null, 
  mapDispatchToProps
)(HomeButton)

export default HomeContainer